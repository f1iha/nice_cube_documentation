# Nice_cube_documentation
Ce projet rassemble de la documentation partagée par le radioclub d'Antibes F6KHK, avec l'AMSAT, l'Université de Nice et le LEAT autour du projet NiceCube.

## Renseignements spécifiques concernant le Projet de F4HDK ditr "NPR"

Pour préparer une maquette du Satellite NiceCube, le radioclub F6KHK a proposé de partir d'une solide base existante constituée par le projet déjà bien abouti NPR de Guillaume F4HDK. 
Cela permet aux étudiants/participants de démarrer sur un existant.

Fonctionnant en GFSK dans notre cas, il permet d'envisager une modulation plus performante que la traditionnelle audio FSK (utilisée couramment avec le protocole AX25)

Il constitue une base hardware existante à base de STM32 qui permet aux développeurs de logiciel de gagner du temps.

Il ne présume pas des choix techniques que l'équipe NiceCube effectuera pour le satellite.

Un essai NPR sur la bande 70cm est en cours sur le site du Mont Agel.


* [Source du projet](https://hackaday.io/project/164092-npr-new-packet-radio) publiée par F4HDK sur le site hackaday.io

* Composants du projet NPR :
    * [Spécification du chip radio SI4463](https://www.silabs.com/wireless/proprietary/ezradiopro-sub-ghz-ics/device.si4463) sur la page du fournissseur.
    * [Spécification du module radio](https://www.nicerf.com/product_153_140.html) intégrant un SI4463 et un amplificateur de 1W.

## Renseignements divers
[Spécification du chip radio sx1262](https://www.semtech.com/products/wireless-rf/lora-transceivers/sx1262) utilisé par le LEAT sur un autre prototype.

